package main

import (
	"archive/zip"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
)

func zipDir(dir, zipPath string) error {
	out, err := os.OpenFile(zipPath, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return fmt.Errorf("Cannot open resulting zip file: %v", err)
	}
	zipOut := zip.NewWriter(out)
	err = filepath.Walk(dir, func(file string, info os.FileInfo, err error) error {
		if err != nil {
			fmt.Printf("prevent panic by handling failure accessing a path %q: %v\n", file, err)
			return err
		}
		if !info.Mode().IsRegular() {
			return nil
		}

		shortName := file[len(dir)+1:]
		f, err := zipOut.Create(shortName)
		if err != nil {
			return fmt.Errorf("Error creating file '%v' in zip archive: %v", info.Name(), err)
		}
		fin, err := os.Open(file)
		if err != nil {
			return fmt.Errorf("Cannot open input file '%v': %v", file, err)
		}
		defer fin.Close()
		_, err = io.Copy(f, fin)
		if err != nil {
			return fmt.Errorf("Cannot copy file '%v' into zip archive: %v", file, err)
		}

		return nil
	})
	if err != nil {
		fmt.Printf("error walking the path %q: %v\n", dir, err)
		return nil
	}
	if err = zipOut.Close(); err != nil {
		return fmt.Errorf("Cannot write zip archive into '%v', %v", out, err)
	}
	return nil
}

func main() {
	dir, err := ioutil.TempDir("", "gocache_")
	if err != nil {
		log.Fatal(err)
	}
	defer func(dir string) {
		if err := os.RemoveAll(dir); err != nil {
			fmt.Fprintf(os.Stderr, "Warning: Cannot remove temp dir '%v': %v\nPlease remove it by yourself", dir, err)
		}
	}(dir)
	fmt.Fprintf(os.Stderr, "tempdir = %v\n", dir)
	env := os.Environ()
	for k, v := range map[string]string{
		"GO111MODULES": "on",
		"GOPROXY":      "direct",
		"GOPATH":       dir,
	} {
		found := false
		for i, e := range env {
			if strings.HasPrefix(e, k+"=") {
				env[i] = k + "=" + v
				found = true
				break
			}
		}
		if !found {
			env = append(env, k+"="+v)
		}
	}
	cmd := exec.Command("go", "mod", "download")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Env = env
	if err := cmd.Run(); err != nil {
		log.Fatalf("go build failed: %v", err)
	}
	if err = zipDir(path.Join(dir, "pkg/mod/cache/download"), "./goproxy_cache.zip"); err != nil {
		log.Fatal(err)
	}
	fmt.Printf("go build cache is stored in ./goproxy_cache.zip")
}
