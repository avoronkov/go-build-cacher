# go-build-cacher

Grab thirdparty modules for your Go project.

## Build / Install

- `go build`

- `go install`

## Usage

- `$ cd your-go-project`

- `$ go-build-cacher`

This will create `goproxy_cache.zip` archive with thirdparty modules.

Put it on your fileserver and unzip it.
Then build your project with:

- `$ env GOPROXY=http://your-fileserver.tld/goproxy/dir go build`

Your dependencies will be downloaded from your fileserver.
